﻿using Store;
using System;
using System.Linq;

namespace StoreMemory
{
    public class BookRepository : IBookRepository
    {
        private readonly Book[] books = new[]
        {
            new Book(1, "Harry Potter", "ISBN 12313-16545", "Joahn Rolling"),
            new Book(2, "The Witcher", "ISBN 44235-67236", "Anjey Sapkovskiy"),
            new Book(3, "Think & Rich", "ISBN 12346-23467", "Napaleon Hill")
        };

        public Book[] GetAllByIsbn(string isbn)
        {
            return books.Where(book => book.Isbn == isbn)
                       .ToArray();
        }

        public Book[] GetAllByTitleOrAuthor(string query)
        {
            return books.Where(book => book.Author.Contains(query)
                               || book.Title.Contains(query))
                                .ToArray();
        }
    }
}
